# Specify the base image
FROM python:3.9-slim-buster

# Set the working directory
WORKDIR /app

# Copy the requirements file to the working directory
COPY src/requirements.txt .

# Install the requirements
RUN pip install  --no-cache-dir -r requirements.txt
