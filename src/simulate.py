import paho.mqtt.client as mqtt
import random
import time

# Definindo broker MQTT
broker_address = "mqtt5"
broker_port = 1883
username = "root"
password = "root"

# Criando e conectando cliente ao broker
client = mqtt.Client()
client.username_pw_set(username, password)
client.connect(broker_address, broker_port)

estadoAspersor = False


def on_message(client, userdata, message):
    global estadoAspersor
    print(f"Recebido o tópico '{message.topic}': {message.payload.decode()}")

    if message.topic == "irrigaja/ligaDesliga":
        if estadoAspersor is False:
            print("Ligando aspersor")
            estadoAspersor = True
        else:
            print("Desligando aspersor")
            estadoAspersor = False

    elif message.topic == "irrigaja/umidadeSolo":
        umidade = random.randint(0, 100)
        msg_retorno = f"Umidade do solo: {umidade}%"
        print(msg_retorno)
        topico_retorno = "irrigaja/retornoUmidadeSolo"
        time.sleep(2)
        client.publish(topico_retorno, msg_retorno)

    elif message.topic == "irrigaja/umidadeAr":
        umidade = random.randint(0, 100)
        msg_retorno = f"Umidade do ar: {umidade}%"
        print(msg_retorno)
        topico_retorno = "irrigaja/retornoUmidadeAr"
        time.sleep(2)
        client.publish(topico_retorno, msg_retorno)

    elif message.topic == "irrigaja/temperatura":
        temperatura = random.randint(5, 50)
        msg_retorno = f"Temperatura: {temperatura}°C"
        print(msg_retorno)
        topico_retorno = "irrigaja/retornoTemperatura"
        time.sleep(2)
        client.publish(topico_retorno, msg_retorno)

    elif message.topic == "irrigaja/velocidadeVento":
        velocidadeVento = random.randint(0, 30)
        msg_retorno = f"Velocidade do vento: {velocidadeVento}km/h"
        print(msg_retorno)
        topico_retorno = "irrigaja/retornoVelocidadeVento"
        time.sleep(2)
        client.publish(topico_retorno, msg_retorno)


client.on_message = on_message

# Se inscrevendo nos tópicos de interesse
topic = "root/root"
client.subscribe(topic)
topic = "root/root2"
client.subscribe(topic)
topic = "irrigaja/ligaDesliga"
client.subscribe(topic)
topic = "irrigaja/umidadeSolo"
client.subscribe(topic)
topic = "irrigaja/umidadeAr"
client.subscribe(topic)
topic = "irrigaja/temperatura"
client.subscribe(topic)
topic = "irrigaja/velocidadeVento"
client.subscribe(topic)

# Loop para escutar mensagens nos tópicos inscritos
client.loop_forever()
