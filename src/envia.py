import paho.mqtt.client as mqtt
import threading

# Definindo porta de comunicação MQTT
broker_port = 1883


# Mensagem de volta
def on_message(client, userdata, message):
    print(f"{message.payload.decode()}")
    client.unsubscribe(message.topic)
    client.disconnect()


# Interrompe a tentativa de ler uma mensagem
# caso 10 segundos se passem sem resposta
def desconectar_apos_tempo(client):
    if not client.desconectado:
        print("Tempo esgotado. Desconectando o cliente.")
        client.disconnect()


def recebe_mqtt_msg(topic):
    client = mqtt.Client()
    client.on_message = on_message
    client.desconectado = False

    # Configurando temporizador para desconectar o cliente após 10 segundos
    temporizador_desconectar = threading.Timer(10, desconectar_apos_tempo,
                                               args=[client])
    temporizador_desconectar.start()

    client.connect("localhost", broker_port)
    client.subscribe(topic)
    client.loop_forever()
    client.desconectado = True


# Função para enviar uma mensagem MQTT
def manda_mqtt_msg(topic, message):
    client = mqtt.Client()
    client.connect("localhost", broker_port)
    client.publish(topic, message)
    client.disconnect()


# Menu interativo
while True:
    print("Menu:")
    print("1. Enviar mensagem MQTT")
    print("2. Ligar/desligar aspersor")
    print("3. Simular recebimento de umidade do solo")
    print("4. Simular recebimento de umidade do ar")
    print("5. Simular recebimento de temperatura")
    print("6. Simular recebimento de velocidade do vento")
    print("7. Sair")

    opcao = input("Escolha uma opção: ")

    if opcao == "1":
        topico = input("Digite o tópico MQTT: ")
        msg = input("Digite a mensagem MQTT: ")
        manda_mqtt_msg(topico, msg)
        print("Mensagem enviada com sucesso!")

    elif opcao == "2":
        topico = "irrigaja/ligaDesliga"
        msg = "1"
        manda_mqtt_msg(topico, msg)
        print("Mensagem enviada com sucesso!")

    elif opcao == "3":
        topico = "irrigaja/umidadeSolo"
        topico_retorno = "irrigaja/retornoUmidadeSolo"
        msg = "1"
        manda_mqtt_msg(topico, msg)
        print("Mensagem enviada com sucesso!")
        recebe_mqtt_msg(topico_retorno)

    elif opcao == "4":
        topico = "irrigaja/umidadeAr"
        topico_retorno = "irrigaja/retornoUmidadeAr"
        msg = "1"
        manda_mqtt_msg(topico, msg)
        print("Mensagem enviada com sucesso!")
        recebe_mqtt_msg(topico_retorno)

    elif opcao == "5":
        topico = "irrigaja/temperatura"
        topico_retorno = "irrigaja/retornoTemperatura"
        msg = "1"
        manda_mqtt_msg(topico, msg)
        print("Mensagem enviada com sucesso!")
        recebe_mqtt_msg(topico_retorno)

    elif opcao == "6":
        topico = "irrigaja/velocidadeVento"
        topico_retorno = "irrigaja/retornoVelocidadeVento"
        msg = "1"
        manda_mqtt_msg(topico, msg)
        print("Mensagem enviada com sucesso!")
        recebe_mqtt_msg(topico_retorno)

    elif opcao == "7":
        print("Saindo do programa.")
        break

    else:
        print("Opção inválida. Tente novamente.")
